---
## How to Install

To install the project on your system copy and paste the following command in the command prompt so that you can connect to the repository and install the project on your system.
```
git clone https://SwathiLizThomas@bitbucket.org/SwathiLizThomas/specialitybeveragespricecalculator.git
```
Or you can also enter **git clone** in the command prompt followed by the URL given below:
```
https://SwathiLizThomas@bitbucket.org/SwathiLizThomas/specialitybeveragespricecalculator.git
```
## How to Compile

Before compiling make sure that JDK (Java Development Kit) is installed in the local system.

You can download and install JDK for your operating system from the following URL:

```
https://www.oracle.com/technetwork/java/javase/downloads/jdk12-downloads-5295953.html
```

To compile the code, run the following command in command prompt.
```
javac SpecialityBeverages.java
```
## How to Run

To execute the code, run the following command in command prompt.
```
java SpecialityBeverages
```
---

## License

**Copyright 2018 Swathi Liz Thomas**

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at [apache.org/licenses/LICENSE-2.0](http://www.apache.org/licenses/LICENSE-2.0)

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

**Reason to choose this license:**

Apache License is a free software license. 
It allows users to use the software for any purpose, to distribute it, to modify it, and to distribute modified versions of the software under the terms of the license, without concern.

Some of the advantages of using this license are:

a) All copies, modified or unmodified, are accompanied by a copy of the licence

b) All modifications are clearly marked as being the work of the modifier

c) All notices of copyright, trademark and patent rights are reproduced accurately in distributed copies
the licensee does not use any trademarks that belong to the licensor

---
## Wiki

**How to view the new Wiki page "Why should Capstone repositories be private not public?" created in the repository**

Go to *Wiki*, select the parent folder *SpecialityBeveragesPriceCalculator*.

This will list the two Wiki pages available in the repository which are: 

*1) Home*

*2) Why should Capstone repositories be private not public?*

Select the second one to view the article created.

---

**Edit a file, create a new file, and clone from Bitbucket in under 2 minutes**

When you're done, you can delete the content in this README and update the file with details for others getting started with your repository.

*We recommend that you open this README in another tab as you perform the tasks below. You can [watch our video](https://youtu.be/0ocf7u76WSo) for a full demo of all the steps in this tutorial. Open the video in a new tab to avoid leaving Bitbucket.*

---

## Edit a file

You’ll start by editing this README file to learn how to edit a file in Bitbucket.

1. Click **Source** on the left side.
2. Click the README.md link from the list of files.
3. Click the **Edit** button.
4. Delete the following text: *Delete this line to make a change to the README from Bitbucket.*
5. After making your change, click **Commit** and then **Commit** again in the dialog. The commit page will open and you’ll see the change you just made.
6. Go back to the **Source** page.

---

## Create a file

Next, you’ll add a new file to this repository.

1. Click the **New file** button at the top of the **Source** page.
2. Give the file a filename of **contributors.txt**.
3. Enter your name in the empty file space.
4. Click **Commit** and then **Commit** again in the dialog.
5. Go back to the **Source** page.

Before you move on, go ahead and explore the repository. You've already seen the **Source** page, but check out the **Commits**, **Branches**, and **Settings** pages.

---

## Clone a repository

Use these steps to clone from SourceTree, our client for using the repository command-line free. Cloning allows you to work on your files locally. If you don't yet have SourceTree, [download and install first](https://www.sourcetreeapp.com/). If you prefer to clone from the command line, see [Clone a repository](https://confluence.atlassian.com/x/4whODQ).

1. You’ll see the clone button under the **Source** heading. Click that button.
2. Now click **Check out in SourceTree**. You may need to create a SourceTree account or log in.
3. When you see the **Clone New** dialog in SourceTree, update the destination path and name if you’d like to and then click **Clone**.
4. Open the directory you just created to see your repository’s files.

Now that you're more familiar with your Bitbucket repository, go ahead and add a new file locally. You can [push your change back to Bitbucket with SourceTree](https://confluence.atlassian.com/x/iqyBMg), or you can [add, commit,](https://confluence.atlassian.com/x/8QhODQ) and [push from the command line](https://confluence.atlassian.com/x/NQ0zDQ).