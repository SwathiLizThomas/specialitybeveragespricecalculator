package com.specialitybeverage;

import java.util.Scanner;

public class SpecialityBeverages
{
    public static void main(String[] args)
    {
        //variables required are declared and initialized
        final double SMALL_SIZE = 1.50, MEDIUM_SIZE = 2.50, LARGE_SIZE = 3.25,
                VANILLA_FLAVOUR = 0.25, CHOCOLATE_FLAVOUR = 0.75,
                LEMON_FLAVOUR = 0.25, MINT_FLAVOUR = 0.50;
        final int TAX_PERCENT = 13;
        String customerName, beverageType, beverageSize, flavour;
        double beverageTotalCost = 0;

        //Scanner object created
        Scanner sc = new Scanner(System.in);

        //try catch block

        try
        {
            System.out.println("\nSWATHI'S COFFEE SHOP" +
                    "\n\nWelcome!!!" +
                    "\n\nEnter your name: ");
            customerName = sc.nextLine();

            System.out.println("The beverages available are:\nCoffee(C) or Tea(T)" +
                    "\nEnter the beverage type you would like to order: ");
            beverageType = sc.nextLine();

            if(beverageType.equalsIgnoreCase("Coffee") || beverageType.equalsIgnoreCase("C"))
                beverageType = "Coffee";
            else if(beverageType.equalsIgnoreCase("Tea") || beverageType.equalsIgnoreCase("T"))
                beverageType = "Tea";
            else
            {
                System.out.println("Invalid type specified\nExit...");
                System.exit(0);
            }

            System.out.println("The sizes of the beverage available are:\nSmall(S), Medium(M) or Large(L)" +
                    "\nEnter the size of the beverage: ");
            beverageSize = sc.nextLine();

            if(beverageSize.equalsIgnoreCase("Small") || beverageSize.equalsIgnoreCase("S"))
            {
                beverageTotalCost += SMALL_SIZE;
                beverageSize = "Small";
            }
            else if(beverageSize.equalsIgnoreCase("Medium") || beverageSize.equalsIgnoreCase("M"))
            {
                beverageTotalCost += MEDIUM_SIZE;
                beverageSize = "Medium";
            }
            else if(beverageSize.equalsIgnoreCase("Large") || beverageSize.equalsIgnoreCase("L"))
            {
                beverageTotalCost += LARGE_SIZE;
                beverageSize = "Large";
            }
            else
            {
                System.out.println("Invalid size specified\n\nExit...");
                System.exit(0);
            }

            if(beverageType.equalsIgnoreCase("Coffee"))
            {
                System.out.println("The flavours available for the coffee are: \nVanilla(V), Chocolate(C) or None");
                System.out.println("Enter the flavour you would like to add to your drink: ");
                flavour = sc.nextLine();
                if(flavour.equalsIgnoreCase("Vanilla") || flavour.equalsIgnoreCase("V"))
                {
                    beverageTotalCost += VANILLA_FLAVOUR;
                    flavour = "Vanilla";
                }
                else if(flavour.equalsIgnoreCase("Chocolate") || flavour.equalsIgnoreCase("C"))
                {
                    beverageTotalCost += CHOCOLATE_FLAVOUR;
                    flavour = "Chocolate";
                }
                else if(flavour.equalsIgnoreCase("None"))
                    flavour = "No";
                else
                {
                    System.out.println("Invalid flavour specified\n\nExit...");
                    System.exit(0);
                }
            }
            else
            {
                System.out.println("The flavours available for the tea are: \nLemon(L), Mint(M) or None");
                System.out.println("Enter the flavour you would like to add to your drink: ");
                flavour = sc.nextLine();
                if(flavour.equalsIgnoreCase("Lemon") || flavour.equalsIgnoreCase("L"))
                {
                    beverageTotalCost += LEMON_FLAVOUR;
                    flavour = "Lemon";
                }
                else if(flavour.equalsIgnoreCase("Mint") || flavour.equalsIgnoreCase("M"))
                {
                    beverageTotalCost += MINT_FLAVOUR;
                    flavour = "Mint";
                }
                else if(flavour.equalsIgnoreCase("None"))
                    flavour = "No";
                else
                {
                    System.out.println("Invalid flavour specified\n\nExit...");
                    System.exit(0);
                }
            }

            beverageTotalCost = beverageTotalCost * (1 + (TAX_PERCENT / 100.0));
            beverageTotalCost = Math.round(beverageTotalCost * 100.0) / 100.0;

            System.out.println("\nFor " + customerName.toUpperCase() + ", a " + beverageSize.toLowerCase() + " "
                    + beverageType.toLowerCase() + ", " + flavour.toLowerCase() + " flavouring, total cost: $"
                    + beverageTotalCost + ".");
        }
        catch(Exception e)
        {
            System.out.println("Try again...");
        }
        finally
        {
            System.out.println("\nEnd");
        }
    }
}
